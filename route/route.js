var express =require('express')
var app=express()
var User= require('../model/user')
var Department= require('../model/department')
var Doctor= require('../model/doctor')
var Booking= require('../model/booking')
var moment = require("moment-timezone");
// get dashboard
app.get('/',(req,res)=>{
    res.render('home',{
        data:[]
    })
})
// user re

app.get('/users',(req,res)=>{
    User.find({}).then((users)=>{
    res.render('users',{
        data:users
    })
})
})

//save user
app.post('/save_user',(req,res)=>{
    console.log(req.body)
    var user_data= new User({
        name:req.body.name,
        phone:req.body.phone,
        user_name:req.body.user_name,
        password:req.body.password
    })
    user_data.save().then((d)=>{
        res.redirect('users')
    })
})
//edit user
app.post('/edit_user',(req,res)=>{ 
    var  data = { 
        name:req.body._name, 
        phone:req.body._phone, 
        user_name:req.body._user_name, 
    } 
     User.findOneAndUpdate({_id:req.body._id},data) .then((d)=>{
        res.redirect('users')
    })
  
})


//Department
app.get('/departments',(req,res)=>{
    Department.find({}).then((depts)=>{
    res.render('department',{
        data:depts
    })
})
})
app.post('/save_dept',(req,res)=>{
     var dept= new Department({
        name:req.body.name,
        desc:req.body.desc,
       
    })
    dept.save().then((d)=>{
        res.redirect('departments')
    })
})
//login
app.post('/check_login',(req,res)=>{
    User.find({user_name:req.body.user_name,password:req.body.password})
    .then((data)=>{
        console.log(data) 
        if(data.length > 0){
            res.render('home',{
         data:data
            } )
        } else {
            res.render('login' )
        }
   
    })
})

app.get('/login',(req,res)=>{ 
            res.render('login' ) 
})
app.post('/login',(req,res)=>{ 
    res.render('login' ) 
})
// Doctor
app.get('/doctors',(req,res)=>{ 
    Doctor.find({}).then((doct)=>{
    Department.find({}).then((dept)=>{
        res.render('doctor',{
            data:dept,
            doct:doct
        } ) 
    }) 
}) 
})

//save doctors
app.post('/save_doctor',(req,res)=>{ 
    var doctor_data= new Doctor({
        name:req.body.name,
        phone:req.body.phone,
        email:req.body.email,
        department_id:req.body.department_id
    })
    doctor_data.save().then((d)=>{
        res.redirect('doctors')
    })
})

 

// Booking get
app.get('/booking',(req,res)=>{ 
    Booking.aggregate([
        {
            $lookup:{
                from:"doctors",
                localField:"doctor_id",
                foreignField:"_id",
                as: "doctor"
            },
        },
            { $unwind: "$doctor" },
        
    ]).then((b)=>{
    Doctor.find({}).then((doct)=>{
        res.render('booking',{
            data:b,
            doct:doct,
            moment:moment
        } ) 
    }) 
}) 
})

//save Booking
app.post('/save_booking',(req,res)=>{ 
    var b_data= new Booking({
        patient_name:req.body.p_name,
        patient_phone:req.body.p_phone, 
        doctor_id:req.body.doctor_id,
        issue:req.body.issue
    })
    b_data.save().then((d)=>{
        res.redirect('booking')
    })
})

 
module.exports =app